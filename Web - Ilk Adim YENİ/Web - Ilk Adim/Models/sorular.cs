//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Web___Ilk_Adim.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class sorular
    {
        public int id_soru { get; set; }
        public int id_ders { get; set; }
        public string konu { get; set; }
        public string resim { get; set; }
        public System.DateTime tarih { get; set; }
        public int cozum { get; set; }
        public int id_user { get; set; }
        public string ders { get; set; }
    }
}
