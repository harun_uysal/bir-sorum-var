﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web___Ilk_Adim.Models;

namespace Web___Ilk_Adim.Controllers
{
    public class AdminController : Controller
    {
        private birsorumvarEntities1 db = new birsorumvarEntities1();

        // GET: uyeler
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UyelerIndex()
        {
            return View(db.uyeler.ToList());
        }
        // GET: uyeler/Details/5
        public ActionResult UyelerDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            uyeler uyeler = db.uyeler.Find(id);
            if (uyeler == null)
            {
                return HttpNotFound();
            }
            return View(uyeler);
        }

        // GET: uyeler/Create
        public ActionResult UyelerCreate()
        {
            return View();
        }

        // POST: uyeler/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UyelerCreate([Bind(Include = "id_user,uye_ad,uye_mail,uye_parola")] uyeler uyeler)
        {
            if (ModelState.IsValid)
            {
                db.uyeler.Add(uyeler);
                db.SaveChanges();
                return RedirectToAction("UyelerIndex");
            }

            return View(uyeler);
        }

        // GET: uyeler/Edit/5
        public ActionResult UyelerEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            uyeler uyeler = db.uyeler.Find(id);
            if (uyeler == null)
            {
                return HttpNotFound();
            }
            return View(uyeler);
        }

        // POST: uyeler/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UyelerEdit([Bind(Include = "id_user,uye_ad,uye_mail,uye_parola")] uyeler uyeler)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uyeler).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("UyelerIndex");
            }
            return View(uyeler);
        }

        // GET: uyeler/Delete/5
        public ActionResult UyelerDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            uyeler uyeler = db.uyeler.Find(id);
            if (uyeler == null)
            {
                return HttpNotFound();
            }
            return View(uyeler);
        }

        // POST: uyeler/Delete/5
        [HttpPost, ActionName("UyelerDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            uyeler uyeler = db.uyeler.Find(id);
            db.uyeler.Remove(uyeler);
            db.SaveChanges();
            return RedirectToAction("UyelerIndex");
        }
        // - DERSLER ADMIN ---------------

        // GET: Admin
        public ActionResult derslerIndex()
        {
            return View(db.dersler.ToList());
        }

        // GET: Admin/Details/5
        public ActionResult derslerDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            dersler dersler = db.dersler.Find(id);
            if (dersler == null)
            {
                return HttpNotFound();
            }
            return View(dersler);
        }

        // GET: Admin/Create
        public ActionResult derslerCreate()
        {
            return View();
        }

        // POST: Admin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult derslerCreate([Bind(Include = "id_ders,ad_ders")] dersler dersler)
        {
            if (ModelState.IsValid)
            {
                db.dersler.Add(dersler);
                db.SaveChanges();
                return RedirectToAction("derslerIndex");
            }

            return View(dersler);
        }

        // GET: Admin/Edit/5
        public ActionResult derslerEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            dersler dersler = db.dersler.Find(id);
            if (dersler == null)
            {
                return HttpNotFound();
            }
            return View(dersler);
        }

        // POST: Admin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult derslerEdit([Bind(Include = "id_ders,ad_ders")] dersler dersler)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dersler).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("derslerIndex");
            }
            return View(dersler);
        }

        // GET: Admin/Delete/5
        public ActionResult derslerDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            dersler dersler = db.dersler.Find(id);
            if (dersler == null)
            {
                return HttpNotFound();
            }
            return View(dersler);
        }

        // POST: Admin/Delete/5
        [HttpPost, ActionName("derslerDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult derslerDeleteConfirmed(int id)
        {
            dersler dersler = db.dersler.Find(id);
            db.dersler.Remove(dersler);
            db.SaveChanges();
            return RedirectToAction("derslerIndex");
        }

        // ------ ADMIN YORUMLAR ----------

        // GET: Admin
        public ActionResult yorumlarIndex()
        {
            return View(db.yorumlar.ToList());
        }

        // GET: Admin/Details/5
        public ActionResult yorumlarDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            yorumlar yorumlar = db.yorumlar.Find(id);
            if (yorumlar == null)
            {
                return HttpNotFound();
            }
            return View(yorumlar);
        }

        // GET: Admin/Create
        public ActionResult yorumlarCreate()
        {
            return View();
        }

        // POST: Admin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult yorumlarCreate([Bind(Include = "id_yorum,id_user,id_soru,yorum,tarih")] yorumlar yorumlar)
        {
            if (ModelState.IsValid)
            {
                db.yorumlar.Add(yorumlar);
                db.SaveChanges();
                return RedirectToAction("yorumlarIndex");
            }

            return View(yorumlar);
        }

        // GET: Admin/Edit/5
        public ActionResult yorumlarEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            yorumlar yorumlar = db.yorumlar.Find(id);
            if (yorumlar == null)
            {
                return HttpNotFound();
            }
            return View(yorumlar);
        }

        // POST: Admin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult yorumlarEdit([Bind(Include = "id_yorum,id_user,id_soru,yorum,tarih")] yorumlar yorumlar)
        {
            if (ModelState.IsValid)
            {
                db.Entry(yorumlar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("yorumlarIndex");
            }
            return View(yorumlar);
        }

        // GET: Admin/Delete/5
        public ActionResult yorumlarDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            yorumlar yorumlar = db.yorumlar.Find(id);
            if (yorumlar == null)
            {
                return HttpNotFound();
            }
            return View(yorumlar);
        }


        // POST: Admin/Delete/5
        [HttpPost, ActionName("yorumlarDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult yorumlarDeleteConfirmed(int id)
        {
            yorumlar yorumlar = db.yorumlar.Find(id);
            db.yorumlar.Remove(yorumlar);
            db.SaveChanges();
            return RedirectToAction("yorumlarIndex");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
