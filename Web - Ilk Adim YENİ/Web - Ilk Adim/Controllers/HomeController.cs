﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Web___Ilk_Adim.Models;
using System.Web.Helpers;

namespace birsorumvar.Controllers
{
    public class HomeController : Controller
    {
        private birsorumvarEntities1 db = new birsorumvarEntities1();
       
        public ActionResult Index()
        {
            Dersler newd = new Dersler();
            newd.dersim = db.dersler.ToList();
            return View(newd);
        }

        public ActionResult Hesap()
        {
            if (Session["S_AD"] == null)
            {
                return RedirectToAction("Kayit", "Home");
            }
            Account newac = new Account();
        
                var userid = Session["S_AD"].ToString();
            newac.uye = db.uyeler.Where(h => h.id_user.ToString() == userid).ToList();

            var res2 = db.yorumlar.OrderByDescending(x => x.tarih).ToList();
            newac.yorumlarim = res2.Where(y => y.id_user.ToString() == userid).ToList();

            newac.sorularim = db.sorular.Where(z => z.id_user.ToString() == userid).ToList();
            return View(newac);
        }


        public ActionResult SoruEkle()
        {
            
            return View();
        }

        public ActionResult Logout()
        {
            if (Session["S_AD"]!=null)
            {
                Session["S_AD"] = null;
            }
            return RedirectToAction("Index", "Home");
        }


        public ActionResult SoruYaz()
        {
            Dersler newd = new Dersler();
            newd.dersim = db.dersler.ToList();
            return View(newd);
        }


        [HttpPost]
        public ActionResult SoruEkle(HttpPostedFileBase uploadfile, FormCollection formdata)
        {

            var user_id = Session["S_AD"];
            birsorumvarEntities1 db = new birsorumvarEntities1();
            sorular tbl = new sorular();
            Dersler newd = new Dersler();
            var dersid = Convert.ToInt32(formdata["ders"]);

            tbl.id_ders = dersid;
            newd.dersim = db.dersler.Where(m => m.id_ders == dersid).ToList();
            //
            var imageName = (from soh in db.dersler
                             where soh.id_ders == dersid
                             select soh.ad_ders).FirstOrDefault();
            //
            tbl.ders = imageName.ToString();
            tbl.konu = formdata["inputkonuad"];
            tbl.cozum = 0;
            tbl.id_user = Convert.ToInt32(user_id);
            tbl.tarih = DateTime.Now;
            tbl.resim = "yol_yok";

            db.sorular.Add(tbl);
            db.SaveChanges();
            int resimid = tbl.id_soru;

            if (uploadfile.ContentLength > 0)
            {
                string filePath = Path.Combine(Server.MapPath("~/img/sorular"), resimid + ".png");
                uploadfile.SaveAs(filePath);

            }
            return View();
        }

        // SORU DETAY RETURN
        public ActionResult SoruDetay()
        {
            string soruid = this.Request.QueryString["soruid"];
            SoruDet newsdet = new SoruDet();
            newsdet.sorumdet = db.sorular.Where(a => a.id_soru.ToString() == soruid).ToList();

            var res = db.yorumlar.OrderByDescending(x => x.tarih).ToList();

            newsdet.sorumyorum = res.Where(b => b.id_soru.ToString() == soruid).ToList();

            return View(newsdet);
        }

        // SORU DETAY YORUM YAZMA RETURN
        [HttpPost]
        public ActionResult SoruDetay(FormCollection yorumform)
        {
            birsorumvarEntities1 db = new birsorumvarEntities1();
            yorumlar tbl = new yorumlar();
            tbl.id_soru = Convert.ToInt32(yorumform["id_soru"]);
            tbl.id_user = Convert.ToInt32(yorumform["id_user"]);
            tbl.yorum = yorumform["comment"];
            tbl.tarih = DateTime.Now;
            db.yorumlar.Add(tbl);
            db.SaveChanges();

            string soruid = this.Request.QueryString["soruid"];
            SoruDet newsdet = new SoruDet();
            newsdet.sorumdet = db.sorular.Where(a => a.id_soru.ToString() == soruid).ToList();
            var res = db.yorumlar.OrderByDescending(x => x.tarih).ToList();
            newsdet.sorumyorum = res.Where(b => b.id_soru.ToString() == soruid).ToList();
            return View(newsdet);

        }



        public ActionResult Sorular()
        {
            string dersid = this.Request.QueryString["id"];
            Soru news = new Soru();

            news.sorum = db.sorular.Where(a => a.id_ders.ToString() == dersid).ToList();
            
            return View(news);
        }

     
        public ActionResult Kayit()
        {
            return View();
        }

        public ActionResult KayitOnay()
        {
            return View();
        }



        [HttpPost]
        public ActionResult Kayit(FormCollection form)
        {
            birsorumvarEntities1 db = new birsorumvarEntities1();
            uyeler tbl = new uyeler();
            tbl.uye_ad = form["uye_ad"].Trim();
            tbl.uye_mail = form["uye_mail"].Trim();
            tbl.uye_parola = form["uye_parola"].Trim();
            db.uyeler.Add(tbl);
            db.SaveChanges();
            return RedirectToAction("KayitOnay", "Home");
        }

        public ActionResult Login()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Login(FormCollection form)
        {
            var mail = form["inputMail"];
            var parola = form["inputParola"];
            var kullanici = db.uyeler.FirstOrDefault(x => x.uye_mail == mail  && x.uye_parola == parola);
            if (kullanici!=null)
            {
                if (mail=="admin@gmail.com" & parola=="admin")
                {
                    Session["S_AD"] = kullanici.id_user;
                    return RedirectToAction("UyelerIndex", "Admin");
                }
                else
                {
                    Session["S_AD"] = kullanici.id_user;
                    return RedirectToAction("Index", "Home");
                }              
            }
            
            return View();
        }

        public class Dersler
        {
            public List<dersler> dersim { get; set; }

        }

        public class Soru
        {
            public List<sorular> sorum { get; set; }
          
        }

        public class SoruDet
        {
            public List<sorular> sorumdet { get; set; }
            public List<yorumlar> sorumyorum { get; set; }
        }

        public class Account
        {
            public List<uyeler> uye { get; set; }
            public List<sorular> sorularim { get; set; }
            public List<yorumlar> yorumlarim { get; set; }
        }

    }
}