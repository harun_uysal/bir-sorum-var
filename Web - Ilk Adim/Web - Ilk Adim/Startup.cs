﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Web___Ilk_Adim.Startup))]
namespace Web___Ilk_Adim
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
